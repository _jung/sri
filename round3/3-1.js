	const Arr = line.split(' ');
	const a = parseInt(Arr[0]);
	const b = parseInt(Arr[1]);
	const n = parseInt(Arr[2]);
	
	if (a === b) {
		console.log(Math.pow(n, a));
	}else{
		let result = 0;
		for (let i = (a > b ? b : a); i <= (a > b ? a : b); i++) {
			result += Math.pow(n, i);
		}
		console.log(result);
	}
