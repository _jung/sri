function solution(s) {
    let sArr = s.split('');
    let sLength = s.length;
    let answer = '';

    if (sLength % 2 == 0) {
        answer = sArr.slice(Math.floor(sLength / 2) - 1, Math.floor(sLength / 2) + 1).join('')
    } else {
        answer = sArr.slice(Math.floor(sLength / 2), Math.floor(sLength / 2) + 1).join('')
    }

    return answer;
}

// function solution(s) {
//     let sLength = s.length;
//     let answer = '';

//     if (sLength % 2 == 0) {
//         answer = s.substring(Math.floor(sLength / 2) - 1, Math.floor(sLength / 2) + 1)
//     } else {
//         answer = s.substring(Math.floor(sLength / 2), Math.floor(sLength / 2) + 1)
//     }

//     return answer;
// }

// 테스트 1 〉	통과(1.56ms, 37.3MB)    테스트 1 〉	통과(1.63ms, 37.2MB)
// 테스트 2 〉	통과(1.67ms, 37.3MB)    테스트 2 〉	통과(1.70ms, 37.6MB)
// 테스트 3 〉	통과(1.61ms, 37.5MB)    테스트 3 〉	통과(1.56ms, 37.4MB)
// 테스트 4 〉	통과(1.60ms, 37.4MB)    테스트 4 〉	통과(1.63ms, 37.4MB)
// 테스트 5 〉	통과(1.67ms, 37.2MB)    테스트 5 〉	통과(1.62ms, 37.5MB)
// 테스트 6 〉	통과(1.60ms, 37.5MB)    테스트 6 〉	통과(1.57ms, 37.4MB)
// 테스트 7 〉	통과(1.64ms, 37.5MB)    테스트 7 〉	통과(1.62ms, 37.4MB)
// 테스트 8 〉	통과(1.70ms, 37.4MB)    테스트 8 〉	통과(1.59ms, 37.3MB)
// 테스트 9 〉	통과(1.61ms, 37.5MB)    테스트 9 〉	통과(1.70ms, 37.3MB)
// 테스트 10 〉	통과(1.57ms, 37.4MB)    테스트 10 〉통과(1.62ms, 37.5MB)
// 테스트 11 〉	통과(1.67ms, 37.4MB)    테스트 11 〉통과(1.71ms, 37.7MB)
// 테스트 12 〉	통과(1.68ms, 37.7MB)    테스트 12 〉통과(1.69ms, 37.4MB)
// 테스트 13 〉	통과(1.67ms, 37.2MB)    테스트 13 〉통과(1.56ms, 37.5MB)
// 테스트 14 〉	통과(1.62ms, 37.3MB)    테스트 14 〉통과(1.64ms, 37.2MB)
// 테스트 15 〉	통과(1.61ms, 37.5MB)    테스트 15 〉통과(1.68ms, 37.4MB)
// 테스트 16 〉	통과(1.69ms, 37.3MB)    테스트 16 〉통과(1.70ms, 37.3MB)