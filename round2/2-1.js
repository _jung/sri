line = Number(line);
function charge(num) {
    let result = '';
    if (num < 100) {
        result = num * (0.5 / 100)
    } else if (num < 200) {
        result = num * (0.7 / 100)
    } else if (num < 300) {
        result = num * (0.9 / 100)
    } else {
        result = num * (1 / 100)
    }
    return result.toFixed(2)
}

console.log(charge(line));