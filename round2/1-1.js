function divisors(num) {
    let result = '';
    for (let i = 1; i <= num; i++) {
        if (num % i === 0) {
            result += i + ' ';
        }
    }
    return result;
}

console.log(divisors(parseInt(line)));